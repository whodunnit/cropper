<?php
/**
 * Cropper
 *
 * @copyright     Copyright (c) 2016 Warrick Bayman.
 * @author        Warrick Bayman <me@warrickbayman.co.za>
 * @license       MIT License http://opensource.org/licenses/MIT
 *
 */

namespace Cropper;

use Intervention\Image\ImageManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class Cropper extends Command
{
    protected function configure()
    {
        $this->setName('crop')
            ->setDescription('Crop the images in the specified directory')
            ->addArgument('directory', InputArgument::REQUIRED, 'The directory containing the images to crop')
            ->addArgument('destination', InputArgument::REQUIRED, 'The destination directory')
            ->addOption('size', 's', InputOption::VALUE_OPTIONAL, 'The size of the thumbnail to generate', 300)
            ->addOption('scale', 't', InputOption::VALUE_OPTIONAL, 'The scale of the image. Width and height separated by a single "x".');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getArgument('directory');
        $target = $input->getArgument('destination');
        $size = intval($input->getOption('size'));
        $scale = $input->getOption('scale');

        if ($scale && $size) {
            $output->writeln('<info>The specified scale value will take precedence over the size.</info>');
        }

        $helper = $this->getHelper('question');
        $questionMessage = 'Crop the JPEG images in <info>' . $source . '</info> and save the results to <info>' . $target . '</info>.' . "\n" . 'Do you want to continue (N/y)?';
        $question = new ConfirmationQuestion($questionMessage, false);

        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('<info>Cancelled by user</info>');
            exit(0);
        }

        if (!file_exists($source)) {
            $output->writeln('<error>Source directory does not exist.</error>');
            exit(1);
        }

        if (!file_exists($target)) {
            mkdir($target);
        }

        if ($target[strlen($target) -1] != '/') {
            $target .= '/';
        }
        if ($target[strlen($source) -1] != '/') {
            $source .= '/';
        }

        $files = glob($source . '*');

        $imageManager = new ImageManager([
            'driver' => 'imagick'
        ]);

        $extensions = [
            'jpe', 'jpeg', 'jpg'
        ];

        foreach ($files as $file) {
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $filename = pathinfo($file, PATHINFO_FILENAME);
            if (in_array(strtolower($ext), $extensions)) {
                $output->write('Cropping ' . $filename . '.' . $ext . '... ');
                $image = $imageManager->make($file);

                if ($scale) {
                    list($width, $height) = str_getcsv($scale, 'x');
                } else {
                    $width = $size;
                    $height = $size;
                }
                if ($image->width() >= $image->height()) {
                    $image->heighten($height);
                }
                if ($image->height() > $image->width()) {
                    $image->widen($width);
                }

                $image->crop($width, $height);
                $image->save($target . $filename . '.' . strtolower($ext), 100);
                $image->destroy();
                $output->writeln('<info>DONE</info>');
            }
        }
    }
}
